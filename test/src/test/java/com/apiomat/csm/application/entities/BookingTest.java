package com.apiomat.csm.application.entities;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BookingTest
{
  /**
   * Test whether booking is correctly constructed.
   */
  @Test
  public void bookingConstructsCorrectly() {

    Long seatId = 123L;
    Instant start = Instant.now();
    Instant end = Instant.now().plus(2, ChronoUnit.DAYS);

    final Booking booking = new Booking(seatId, start, end);

    assertEquals(seatId, booking.getSeatId(), "seatId not correctly constructed");
    assertEquals(start, booking.getStart(), "start not correctly constructed");
    assertEquals(end, booking.getEnd(), "end not correctly constructed");
  }

  /**
   * Checks whether intersections of bookings are correctly detected by {@link Booking#blocks}.
   */
  @Test
  void bookingBlocking() {
    String message = "booking not correctly blocked";
    Instant now = Instant.now();

    // intersection - same period
    // |--------|
    // |--------|
    Booking booking1 = new Booking(123L,
      now,
      now.plus(2, ChronoUnit.DAYS));
    Booking booking2 = new Booking(123L,
      now,
      now.plus(2, ChronoUnit.DAYS));
    assertTrue(booking1.blocks(booking2),message);

    // intersection - first starts earlier
    // |--------|
    //     |--------|
    booking1 = new Booking(123L,
      now.minus(3, ChronoUnit.DAYS),
      Instant.now().plus(1, ChronoUnit.HOURS));
    booking2 = new Booking(123L,
      now,
      now.plus(2, ChronoUnit.DAYS));
    assertTrue(booking1.blocks(booking2), message);

    // intersection - second starts earlier
    //     |--------|
    // |--------|
    booking1 = new Booking(123L,
      now.minus(1, ChronoUnit.DAYS),
      now.plus(1, ChronoUnit.DAYS));
    booking2 = new Booking(123L,
      now.minus(2, ChronoUnit.DAYS),
      now);
    assertTrue(booking1.blocks(booking2), message);

    // intersection - first period inside second period
    //     |---|
    // |-----------|
    booking1 = new Booking(123L,
      now.minus(1, ChronoUnit.HOURS),
      now.plus(1, ChronoUnit.HOURS));
    booking2 = new Booking(123L,
      now.minus(2, ChronoUnit.HOURS),
      now.plus(2, ChronoUnit.HOURS));
    assertTrue(booking1.blocks(booking2), message);

    // intersection - second period inside first period
    // |-----------|
    //     |---|
    booking1 = new Booking(123L,
      now.minus(1, ChronoUnit.DAYS),
      now.plus(1, ChronoUnit.DAYS));
    booking2 = new Booking(123L,
      now.minus(1, ChronoUnit.SECONDS),
      now);
    assertTrue(booking1.blocks(booking2), message);

    // no intersection same period but different seat
    // |--------| 123
    // |--------| 124
    booking1 = new Booking(123L,
      now,
      now.plus(2, ChronoUnit.DAYS));
    booking2 = new Booking(124L,
      now,
      now.plus(2, ChronoUnit.DAYS));
    assertFalse(booking1.blocks(booking2),message);

    // no intersection - first ends on day earlier than second starts
    // |--------|
    //              |--------|
    booking1 = new Booking(123L,
      now.minus(3, ChronoUnit.DAYS),
      now.minus(1, ChronoUnit.DAYS));
    booking2 = new Booking(123L,
      now,
      now.plus(2, ChronoUnit.DAYS));
    assertFalse(booking1.blocks(booking2), message);

    // no intersection - seconds ends on day earlier than first starts
    //              |--------|
    // |--------|
    booking1 = new Booking(123L,
      now,
      now.plus(2, ChronoUnit.DAYS));
    booking2 = new Booking(123L,
      now.minus(3, ChronoUnit.DAYS),
      now.minus(1, ChronoUnit.DAYS));
    assertFalse(booking1.blocks(booking2), message);

    // no intersection - first ends exactly when second starts
    // |--------|
    //          |--------|
    booking1 = new Booking(123L,
      now.minus(1, ChronoUnit.DAYS),
      now);
    booking2 = new Booking(123L,
      now,
      now.plus(1, ChronoUnit.DAYS));
    assertFalse(booking1.blocks(booking2), message);

    // no intersection - second ends exactly when first starts
    //          |--------|
    // |--------|
    booking1 = new Booking(123L,
      now,
      now.plus(1, ChronoUnit.DAYS));
    booking2 = new Booking(123L,
      now.minus(1, ChronoUnit.DAYS),
      now);
    assertFalse(booking1.blocks(booking2), message);
  }

  /**
   * Checks whether two equal bookings are detected as equal by {@link Booking#equals} and by
   * {@link Booking#hashCode()}.
   */
  @Test
  void bookingEquality() {

    Instant now = Instant.now();

    Booking booking1 = new Booking(123L,
      now, now.plus(1, ChronoUnit.DAYS));
    Booking booking2 = new Booking(123L,
      now, now.plus(1, ChronoUnit.DAYS));

    assertNotSame(booking1, booking2);
    assertEquals(booking2, booking1, "booking are not equal");
    assertEquals(booking1.hashCode(), booking2.hashCode(), "booking hash codes are not equal");
  }

  /**
   * Checks whether two unequal bookings are detected as unequal by {@link Booking#equals} and by
   * {@link Booking#hashCode()}.
   */
  @Test
  void bookingInequality() {

    Instant now = Instant.now();
    String equalsMessage = "booking equality incorrect";
    String hashCodeMessage = "booking hash code equality incorrect";

    // different seat
    Booking booking1 = new Booking(123L,
      now.minus(1, ChronoUnit.DAYS), now.plus(1, ChronoUnit.DAYS));
    Booking booking2 = new Booking(124L,
      now.minus(1, ChronoUnit.DAYS), now.plus(1, ChronoUnit.DAYS));

    assertNotSame(booking1, booking2);
    assertNotEquals(booking2, booking1, equalsMessage);
    assertNotEquals(booking1.hashCode(), booking2.hashCode(), hashCodeMessage);

    // different start
    booking1 = new Booking(123L,
      now.minus(1, ChronoUnit.DAYS), now.plus(1, ChronoUnit.DAYS));
    booking2 = new Booking(123L,
      now.minus(2, ChronoUnit.DAYS), now.plus(1, ChronoUnit.DAYS));

    assertNotSame(booking1, booking2);
    assertNotEquals(booking2, booking1, equalsMessage);
    assertNotEquals(booking1.hashCode(), booking2.hashCode(), hashCodeMessage);

    // different end
    booking1 = new Booking(123L,
      now.minus(1, ChronoUnit.DAYS), now.plus(1, ChronoUnit.DAYS));
    booking2 = new Booking(123L,
      now.minus(1, ChronoUnit.DAYS), now.plus(2, ChronoUnit.DAYS));

    assertNotSame(booking1, booking2);
    assertNotEquals(booking2, booking1, equalsMessage);
    assertNotEquals(booking1.hashCode(), booking2.hashCode(), hashCodeMessage);
  }
}
