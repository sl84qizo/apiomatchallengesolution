package com.apiomat.csm.application.services;

import com.apiomat.csm.application.entities.Booking;
import com.apiomat.csm.application.repisitories.BookingRepository;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BookingServiceTest
{
  private final BookingRepository bookingRepository = mock(BookingRepository.class);

  private final BookingService bookingService = new BookingService(bookingRepository);

  /**
   * Test {@link BookingService#validate} to correctly throw an {@link IllegalArgumentException} when
   * {@link Booking#getSeatId()} is null.
   */
  @Test
  public void validateThrowsForNonExistingId( )
  {
    // when
    Booking booking = new Booking(null, Instant.now(), Instant.now().plus(2, ChronoUnit.DAYS));

    final IllegalArgumentException exception = assertThrows( IllegalArgumentException.class, ( ) -> {
      this.bookingService.validate(booking);
    } );

    // then
    assertEquals(exception.getMessage(), "seatId must not be null");
  }

  /**
   * Test {@link BookingService#validate} to correctly throw an {@link IllegalArgumentException} when
   * {@link Booking#getStart()} is null.
   */
  @Test
  public void validateThrowsForNonExistingStart( )
  {
    // when
    Booking booking = new Booking(123L, null, Instant.now());

    final IllegalArgumentException exception = assertThrows( IllegalArgumentException.class, ( ) -> {
      this.bookingService.validate(booking);
    } );

    // then
    assertEquals(exception.getMessage(), "start must not be null");
  }

  /**
   * Test {@link BookingService#validate} to correctly throw an {@link IllegalArgumentException} when
   * {@link Booking#getEnd()} is null.
   */
  @Test
  public void validateThrowsForNonExistingEnd( )
  {
    // when
    Booking booking = new Booking(123L, Instant.now(), null);

    final IllegalArgumentException exception = assertThrows( IllegalArgumentException.class, ( ) -> {
      this.bookingService.validate(booking);
    } );

    // then
    assertEquals(exception.getMessage(), "end must not be null");
  }

  /**
   * Test {@link BookingService#validate} to correctly throw an {@link IllegalArgumentException} when the
   * timestamp of {@link Booking#getEnd()} is smaller than {@link Booking#getStart()}.
   */
  @Test
  public void validateThrowsForEndBeforeStart( )
  {
    // when
    Booking booking = new Booking(123L, Instant.now(), Instant.now().minus(1, ChronoUnit.DAYS));

    final IllegalArgumentException exception = assertThrows( IllegalArgumentException.class, ( ) -> {
      this.bookingService.validate(booking);
    } );

    // then
    assertEquals(exception.getMessage(), "end must not be after start");
    // hint: 'end must be after start' would be a better message
  }

  /**
   * Test {@link BookingService#validate} to correctly throw an {@link IllegalArgumentException} when
   * {@link BookingRepository#existsBlockingBooking} returns true. This means that the seat is already taken.
   */
  @Test
  public void validateThrowsForSeatTaken( )
  {
    // when
    Booking booking = new Booking(123L, Instant.now(), Instant.now().plus(1, ChronoUnit.DAYS));
    when(bookingRepository.existsBlockingBooking(booking)).thenReturn(true);

    final IllegalArgumentException exception = assertThrows( IllegalArgumentException.class, ( ) -> {
      this.bookingService.validate(booking);
    } );

    // then
    assertEquals(exception.getMessage(), "seat is already taken within your booking period");
  }
}
