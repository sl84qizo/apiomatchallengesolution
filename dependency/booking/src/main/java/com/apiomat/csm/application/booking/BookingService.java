package com.apiomat.csm.application.booking;


import java.util.ArrayList;
import java.util.List;

public class BookingService
{
	private final List<BookingListener> listeners = new ArrayList<>();

	public void create( final Booking booking )
	{
		System.out.println( "user sent booking: " + booking );
		bookingEventFired(booking);
	}

	private void bookingEventFired( final Booking booking ) {
		for (BookingListener listener : listeners) {
			listener.bookingPerformed(new BookingEvent(this, booking));
		}
	}

	public void addBookingListener( BookingListener listener ) {
		listeners.add(listener);
	}

	public void removeBookingListener( BookingListener listener ) {
		listeners.remove(listener);
	}

}
