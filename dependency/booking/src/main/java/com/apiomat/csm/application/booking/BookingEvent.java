package com.apiomat.csm.application.booking;

import java.util.EventObject;

public class BookingEvent extends EventObject {

  private final Booking booking;

  public BookingEvent(Object source, Booking booking) {
    super(source);
    this.booking = booking;
  }

  public Booking getBooking() {
    return booking;
  }
}
