package com.apiomat.csm.application.booking;

public interface BookingListener extends java.util.EventListener {

  void bookingPerformed(BookingEvent e);
}
