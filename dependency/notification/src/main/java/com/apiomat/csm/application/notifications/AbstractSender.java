package com.apiomat.csm.application.notifications;

import com.apiomat.csm.application.booking.Booking;
import com.apiomat.csm.application.booking.BookingEvent;
import com.apiomat.csm.application.booking.BookingListener;

public abstract class AbstractSender implements BookingListener {

  public abstract void send( final Booking booking );

  @Override
  public void bookingPerformed(BookingEvent e) {
    send(e.getBooking());
  }
}
