package com.apiomat.csm.application.notifications;

import com.apiomat.csm.application.booking.Booking;

public class PushSender extends AbstractSender
{
	@Override
	public void send( final Booking booking )
	{
		System.out.println( "sending push message for: " + booking );
	}
}
