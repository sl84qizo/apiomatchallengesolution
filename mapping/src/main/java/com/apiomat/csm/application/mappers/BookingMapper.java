package com.apiomat.csm.application.mappers;

import com.apiomat.csm.application.dtos.BookingDto;
import com.apiomat.csm.application.dtos.blocks.BookingBlockDto;
import com.apiomat.csm.application.dtos.blocks.ImageBlockDto;
import com.apiomat.csm.application.dtos.blocks.TextBlockDto;
import com.apiomat.csm.application.entities.Booking;
import com.apiomat.csm.application.entities.blocks.BookingBlock;
import com.apiomat.csm.application.entities.blocks.ImageBlock;
import com.apiomat.csm.application.entities.blocks.TextBlock;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;

import java.util.Collections;


public class BookingMapper
{
	private final ModelMapper modelMapper = new ModelMapper();

	public BookingMapper() {
		initMapper();
	}

	/**
   * Initialize the type mapping of the {@link #modelMapper} so that Entities can be correctly mapped to
	 * DTOs.
	 */
	public void initMapper() {
		// this issue describes how to do a mapping of a list containing inherited types:
		// https://github.com/modelmapper/modelmapper/issues/182

		// include the subClasses ImageBlock and TextBlock as possible mappings for the baseClass BookingBlock
		modelMapper.createTypeMap(BookingBlock.class, BookingBlockDto.class)
			.include(ImageBlock.class, BookingBlockDto.class)
			.include(TextBlock.class, BookingBlockDto.class);

		// make sure that the fields of the subClasses are correctly mapped
		// a) for ImageBlocks
		modelMapper.typeMap(ImageBlock.class, BookingBlockDto.class).setProvider(new Provider<BookingBlockDto>() {
			public BookingBlockDto get(Provider.ProvisionRequest<BookingBlockDto> request) {
				ImageBlockDto imageBlockDto = new ImageBlockDto();
				modelMapper.map(request.getSource(), imageBlockDto);
				return imageBlockDto;
			}
		});
		// b) for TextBlocks
		modelMapper.typeMap(TextBlock.class, BookingBlockDto.class).setProvider(new Provider<BookingBlockDto>() {
			public BookingBlockDto get(ProvisionRequest<BookingBlockDto> request) {
				TextBlockDto textBlockDto = new TextBlockDto();
				modelMapper.map(request.getSource(), textBlockDto);
				return textBlockDto;
			}
		});
	}

	public BookingDto map( final Booking booking )
	{
		BookingDto bookingDto = modelMapper.map(booking, BookingDto.class);
		Collections.sort(bookingDto.getBlocks());

		return bookingDto;
	}

	public Booking map( final BookingDto bookingDto )
	{
		// Nicht Teil dieser Aufgabe
		return new Booking( );
	}
}
