package com.apiomat.csm.application.dtos.blocks;

public class BookingBlockDto implements Comparable<BookingBlockDto>
{
	private int sortIndex;

	@Override
	public int compareTo(BookingBlockDto o) {
		return Integer.compare(sortIndex, o.sortIndex);
	}

	public int getSortIndex( )
	{
		return this.sortIndex;
	}

	public void setSortIndex( final int sortIndex )
	{
		this.sortIndex = sortIndex;
	}
}
