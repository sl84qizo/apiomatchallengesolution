package com.apiomat.csm.application.services;

import com.apiomat.csm.application.dtos.BookingDto;
import com.apiomat.csm.application.entities.Booking;
import com.apiomat.csm.application.mappers.BookingMapper;
import com.apiomat.csm.application.repositories.BookingRepository;

import java.util.List;
import java.util.stream.Collectors;

public class BookingService
{
	private final BookingRepository repository;
	private final BookingMapper mapper;

	public BookingService( final BookingRepository repository, final BookingMapper mapper )
	{
		this.repository = repository;
		this.mapper = mapper;
	}

	public List<BookingDto> list( )
	{
		final List<Booking> bookings = this.repository.all( );

		return bookings.stream()
			.map(mapper::map)
			.collect(Collectors.toList());
	}

	public BookingDto findById( final Long id )
	{
		final Booking booking = this.repository.findBy( id );

		if (booking == null){
			throw new IllegalStateException("not found");
		}

		return mapper.map(booking);
	}

	public BookingDto create( final BookingDto bookingDto )
	{
		final Booking create = this.mapper.map( bookingDto );
		this.repository.create( create );
		return this.mapper.map( create );
	}
}
